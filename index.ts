#! /usr/bin/env node

const yargs = require('yargs'); // eslint-disable-line
import { startMockServer, getSettings, startNgServe } from './functions';

export const settingsFileName = './.my-cli.json';
export const proxyConfFileName = './.my-proxy.conf.json';
const defaultRestApiPort = 3000;
const defaultDevServerPort = 4200;
const defaultLiveReloadPort = 49200;

async function serve(args: any) {
  const settings = await getSettings();
  const restApiPort =
    (settings.restApiPort || defaultRestApiPort) + settings.id;
  const devServerPort =
    (settings.devServerPort || defaultDevServerPort) + settings.id;
  const liveReloadPort =
    (settings.liveReloadPort || defaultLiveReloadPort) + settings.id;

  startMockServer(restApiPort);
  return startNgServe(args, devServerPort, restApiPort, liveReloadPort);
}

yargs.command({
  command: 'serve [--aot] [--default]',
  describe: 'start local dev server',
  aliases: ['s'],
  handler: args =>
    serve(args).catch(e => {
      console.log(e);
      process.exitCode = 1;
    })
}).argv;
