"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("./index");
var hjson_1 = require("hjson");
var fs = require('fs');
var express = require('express');
var shell = require('shelljs');
function getSettings() {
    return __awaiter(this, void 0, void 0, function () {
        var settings;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, readFile(index_1.settingsFileName, 'utf8')];
                case 1:
                    settings = _a.sent();
                    return [2 /*return*/, hjson_1.parse(settings)];
            }
        });
    });
}
exports.getSettings = getSettings;
function readFile(path, options) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (ok, reject) {
                    fs.readFile(path, options, function (err, data) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            ok(data.toString());
                        }
                    });
                })];
        });
    });
}
exports.readFile = readFile;
function startMockServer(port) {
    var app = express();
    setupMockServer(app);
    return app.listen(port, function () {
        console.log("MockServer listening on port " + port);
    });
}
exports.startMockServer = startMockServer;
function setupMockServer(app) {
    var _this = this;
    app.get(/\/rest\/\S{1,}/, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var fileStr;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    console.log('get: ' + req.originalUrl);
                    return [4 /*yield*/, readFile('./mock/' + req.originalUrl.split('?')[0] + '/get.json', 'utf8')];
                case 1:
                    fileStr = _a.sent();
                    res.json(JSON.parse(fileStr));
                    return [2 /*return*/];
            }
        });
    }); });
    app.post(/\/rest\/\S{1,}/, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var fileStr;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    console.log('post: ' + req.originalUrl);
                    return [4 /*yield*/, readFile('./mock/' + req.originalUrl.split('?')[0] + '/post.json', 'utf8')];
                case 1:
                    fileStr = _a.sent();
                    res.json(JSON.parse(fileStr));
                    return [2 /*return*/];
            }
        });
    }); });
}
function startNgServe(args, devServerPort, restApiPort, liveReloadPort) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, saveProxyConf(restApiPort)];
                case 1:
                    _a.sent();
                    return [2 /*return*/, shell.exec('ng serve ' +
                            (args.aot ? ' --aot' : '') +
                            ' --host 0.0.0.0 --port ' +
                            devServerPort +
                            ' --proxy-config ' +
                            index_1.proxyConfFileName +
                            (args.default ? '' : ' --configuration=local'), { async: true })];
            }
        });
    });
}
exports.startNgServe = startNgServe;
function saveProxyConf(proxyPort) {
    return __awaiter(this, void 0, void 0, function () {
        var requiredProxyConf, currentProxyConf, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    requiredProxyConf = JSON.stringify({
                        '/rest': {
                            target: 'http://localhost:' + proxyPort,
                            secure: false,
                            pathRewrite: { '.php': '' }
                        }
                    });
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, readFile(index_1.proxyConfFileName)];
                case 2:
                    currentProxyConf = _a.sent();
                    if (JSON.stringify(requiredProxyConf) !==
                        JSON.stringify(hjson_1.parse(currentProxyConf))) {
                        return [2 /*return*/, writeFile(index_1.proxyConfFileName, requiredProxyConf)];
                    }
                    return [3 /*break*/, 4];
                case 3:
                    e_1 = _a.sent();
                    return [2 /*return*/, writeFile(index_1.proxyConfFileName, requiredProxyConf)];
                case 4: return [2 /*return*/];
            }
        });
    });
}
function writeFile(path, data, options) {
    return new Promise(function (resolve, reject) {
        return fs.writeFile(path, data, options, function (err) {
            if (err) {
                reject(err);
            }
            resolve();
        });
    });
}
