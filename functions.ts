import { settingsFileName, proxyConfFileName } from './index';
import { parse } from 'hjson';

const fs = require('fs');
const express = require('express');
const shell = require('shelljs');

export async function getSettings(): Promise<{
  titile: string;
  id: number;
  restApiPort: number;
  devServerPort: number;
  liveReloadPort: number;
}> {
  const settings = await readFile(settingsFileName, 'utf8');
  return parse(settings);
}

export async function readFile(
  path: string,
  options?: string
): Promise<string> {
  return new Promise((ok, reject) => {
    fs.readFile(path, options, (err, data) => {
      if (err) {
        reject(err);
      } else {
        ok(data.toString());
      }
    });
  });
}

export function startMockServer(port: number) {
  const app = express();
  setupMockServer(app);
  return app.listen(port, function() {
    console.log(`MockServer listening on port ${port}`);
  });
}

function setupMockServer(app: any): void {
  app.get(/\/rest\/\S{1,}/, async (req, res) => {
    console.log('get: ' + req.originalUrl);
    var fileStr = await readFile(
      './mock/' + req.originalUrl.split('?')[0] + '/get.json',
      'utf8'
    );
    res.json(JSON.parse(fileStr));
  });
  app.post(/\/rest\/\S{1,}/, async (req, res) => {
    console.log('post: ' + req.originalUrl);
    var fileStr = await readFile(
      './mock/' + req.originalUrl.split('?')[0] + '/post.json',
      'utf8'
    );
    res.json(JSON.parse(fileStr));
  });
}

export async function startNgServe(
  args: any,
  devServerPort: number,
  restApiPort: number,
  liveReloadPort: number
): Promise<void> {
  await saveProxyConf(restApiPort);
  return shell.exec(
    'ng serve ' +
      (args.aot ? ' --aot' : '') +
      ' --host 0.0.0.0 --port ' +
      devServerPort +
      ' --proxy-config ' +
      proxyConfFileName +
      (args.default ? '' : ' --configuration=local'),
    { async: true }
  );
}

async function saveProxyConf(proxyPort: number): Promise<void> {
  const requiredProxyConf = JSON.stringify({
    '/rest': {
      target: 'http://localhost:' + proxyPort,
      secure: false,
      pathRewrite: { '.php': '' }
    }
  });
  try {
    const currentProxyConf = await readFile(proxyConfFileName);
    if (
      JSON.stringify(requiredProxyConf) !==
      JSON.stringify(parse(currentProxyConf))
    ) {
      return writeFile(proxyConfFileName, requiredProxyConf);
    }
  } catch (e) {
    return writeFile(proxyConfFileName, requiredProxyConf);
  }
}

function writeFile(
  path: string,
  data: string,
  options?: { encoding?: string; mode?: number; flag?: string }
): Promise<void> {
  return new Promise<void>((resolve, reject) =>
    fs.writeFile(path, data, options, err => {
      if (err) {
        reject(err);
      }
      resolve();
    })
  );
}
